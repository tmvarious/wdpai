<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Routing::get('', 'DefaultController');
Routing::get('home', 'DefaultController');
Routing::get('logout', 'DefaultController');
Routing::post('login', 'SecurityController');
Routing::post('register', 'SecurityController');
Routing::post('cases', 'CaseController');
Routing::post('collections', 'CollectionController');
Routing::post('skins', 'SkinsController');
Routing::post('addCase', 'CaseController');
Routing::post('addCollection', 'CollectionController');
Routing::post('addSkin', 'SkinsController');
Routing::post('search', 'SkinsController');


Routing::run($path);