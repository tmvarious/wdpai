const search = document.querySelector('input[placeholder="search skin"]');
const skinsContainer = document.querySelector(".skins");

search.addEventListener("keyup", function (event){
    if(event.key === "Enter"){
        event.preventDefault();

        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response){
            return response.json();
        }).then(function (skins){
           skinsContainer.innerHTML = "";
           loadSkins(skins);
        });
    }
});


function loadSkins(skins) {
    skins.forEach(skin => {
        console.log(skin);
        createSkin(skin);
    });
}

function createSkin(skin){

    const template = document.querySelector("#skin-template");

    const clone = template.content.cloneNode(true);

    const div = clone.querySelector("div");
    div.id = skin.id;

    const title = clone.querySelector("h2");
    title.innerHTML = skin.title;

    const image = clone.querySelector("img");
    image.src = `/public/uploads/${skin.image}`;

    const price = clone.querySelector("h3");
    price.innerText = skin.price;

    skinsContainer.appendChild(clone);
}