<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/adding.css">

    <script src="https://kit.fontawesome.com/b1d4ada10c.js" crossorigin="anonymous"></script>
    <title>HOME</title>
</head>

<body>
    <div class="base-container">

        <main>
            <header>
                <div class="home-button">

                    <a href="home" class="button">HOME</a>
                </div>
                <div class="cases-button">

                    <a href="cases" class="button">CASES</a>
                </div>
                <div class="collections-button">

                    <a href="collections" class="button">COLLECTIONS</a>
                </div>
                <div class="inventory-button">

                    <a href="inventory" class="button">INVENTORY</a>
                </div>
                <div class="logout-button">

                    <a href="login" class="button">LOGOUT</a>
                </div>
            </header>
            <section class="addingCases">
                <div>
                    <h2>Add new case</h2>
                    <form action="addCase" method="post" enctype="multipart/form-data">
                            <?php
                            if(isset($messages)){
                                foreach($messages as $message) {
                                    echo $message;
                                }
                            }
                            ?>
                        <input name="title" type="text" placeholder="Set new case name">
                        <h2>Set new case image</h2>
                        <input type="file" name="file">
                        <button type="add">ADD CASE</button>
                    </form>
                </div>
            </section>
        </main>
        
    </div>
</body>