<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/adding.css">
    <title>ADD-SKIN</title>
</head>

<body>
    <div class="base-container">

        <main>
            <?php include('header.php')?>
            <section class="addingSkins">
                <div>
                    <h2>Add new skin</h2>
                    <form action="addSkin" method="post" enctype="multipart/form-data">
                            <?php
                            if(isset($messages)){
                                foreach($messages as $message) {
                                    echo $message;
                                }
                            }
                            ?>
                        <input name="title" type="text" placeholder="Set new skin name">
                        <h2>Set price</h2>
                        <input name="price" type="number" step="0.01" placeholder="Set price">
                        <h2>Set new skin image</h2>
                        <input type="file" name="file">
                        <button type="add">ADD SKIN</button>
                    </form>
                </div>
            </section>
        </main>
        
    </div>
</body>