<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/casesAndCollections.css">
    <script src="https://kit.fontawesome.com/b1d4ada10c.js" crossorigin="anonymous"></script>
    <title>HOME</title>
</head>

<body>
    <div class="base-container">

        <main>
            <?php include('header.php')?>
            <addButton>
                <a href="addCase" id="addButton" class="button">
                    ADD NEW CASE
                </a>
            </addButton>
            <section class="boxes">
                <?php foreach($cases as $case): ?>
                <a href="cases" class="button">
                    <div id="cases">
                        <h4><?= $case->getTitle() ?></h4>
                        <img src="public/uploads/<?= $case->getImage() ?>">
                    </div>
                </a>
                <?php endforeach; ?>
            </section>
        </main>
        
    </div>
</body>