<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/casesAndCollections.css">
    <title>HOME</title>
</head>

<body>
    <div class="base-container">

        <main>
            <?php include('header.php')?>
            <addButton>
                <a href="addCollection" id="addButton" class="button">
                    ADD NEW COLLECTION
                </a>
            </addButton>
            <section class="boxes">
                <?php foreach($collections as $collection): ?>
                <a href="collections" class="button">
                    <div id="collections">
                        <h4><?= $collection->getTitle() ?></h4>
                        <img src="public/uploads/<?= $collection->getImage() ?>">
                    </div>
                </a>
                <?php endforeach; ?>
            </section>
        </main>
        
    </div>
</body>