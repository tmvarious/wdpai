<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <title>header</title>
</head>


<header>
    <div class="home-button">

        <a href="home" class="button">HOME</a>
    </div>
    <div class="cases-button">

        <a href="cases" class="button">CASES</a>
    </div>
    <div class="collections-button">

        <a href="collections" class="button">COLLECTIONS</a>
    </div>
    <div class="skins-button">

        <a href="skins" class="button">SKINS</a>
    </div>
    <div class="inventory-button">

        <a href="inventory" class="button">INVENTORY</a>
    </div>
    <div class="logout-button">

        <a href="logout" class="button">LOGOUT</a>
    </div>
</header>
