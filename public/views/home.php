

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/home.css">

    <title>HOME</title>
</head>

<body>
    <div class="base-container">

        <main>
            <?php include('header.php')?>
            <section class="boxes">
                <a href="cases" class="button">
                    <div id="cases">
                        <h4>CASES</h4>
                        <img src="public/img/256fx256f.png">
                    </div>
                </a>
                <a href="collections" class="button">
                    <div id="collections">
                        <h4>COLLECTIONS</h4>
                        <img src="public/img/Canals_Pin.png">
                    </div>
                </a>
                <a href="skins" class="button">
                    <div id="skins">
                        <h4>SKINS</h4>
                        <img src="public/uploads/ak-redline.png">
                    </div>
                </a>
                <a href="inventory" class="button">
                    <div id="inventory">
                        <h4>INVENTORY</h4>
                            <img src="public/img/inv.svg">
                    </div>
                </a>
            </section>
        </main>
        
    </div>
</body>