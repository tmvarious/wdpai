<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/skins.css">

    <script type="text/javascript" src="./public/js/search.js" defer></script>
    <title>SKINS</title>
</head>

<body>
    <div class="base-container">

        <main>
            <?php include('header.php')?>
            <div>
                <div>
                <input placeholder="search skin" id="search">
                </div>
            <addButton>
                <a href="addSkin" id="addButton" class="button">
                    ADD NEW SKIN
                </a>
            </addButton>
            </div>
            <section class="skins">
                <?php foreach($skins as $skin): ?>
                <div>
                    <div id="cases">
                        <h2><?= $skin->getTitle() ?></h2>
                        <img src="public/uploads/<?= $skin->getImage() ?>">
                        <h3><?= $skin->getPrice() ?> $</h3>
                        <a href="#" id="addToInventory" class="button">ADD TO INVENTORY</a>
                    </div>
                </div>
                <?php endforeach; ?>
            </section>
        </main>
        
    </div>
</body>

<template id="skin-template">
    <div>
        <div id="cases">
            <h2>title</h2>
            <img src="">
            <h3>price</h3>
            <a href="#" id="addToInventory" class="button">ADD TO INVENTORY</a>
        </div>
    </div>
</template>
