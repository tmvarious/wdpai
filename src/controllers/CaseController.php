<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/Box.php';
require_once __DIR__ . '/../repo/BoxRepo.php';


class CaseController extends AppController {


    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private $boxRepo;



    public function __construct()
    {
        parent::__construct();
        $this->boxRepo = new BoxRepo();
    }

    public function addCase()
    {

        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name']
            );

            $box = new Box($_POST['title'], $_FILES['file']['name']);
            $this->boxRepo->addCase($box);


            $this->render('cases', [
                'cases' => $this->boxRepo->getBoxes(),
                'message' => $this->messages
            ]);
        } else {
            $this->render('add-case', ['message' => $this->messages]);
        }
    }

    private function validate(array $file): bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'FILE IS TOO LARGE!';
            return false;
        }

        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'FILE TYEP IS NOT SUPPORTED!';
            return false;
        }
        return true;
    }

    public function cases(){
        $boxes = $this->boxRepo->getBoxes();
        $this->render('cases', ['cases' =>$boxes]);
    }
}