<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/Collections.php';
require_once __DIR__ . '/../repo/CollectionsRepo.php';

class CollectionController extends AppController {

    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private $collectionsRepo;


    public function __construct()
    {
        parent::__construct();
        $this->collectionsRepo = new CollectionsRepo();
    }


    public function addCollection()
    {
        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name']
            );

            $collection = new Collections($_POST['title'], $_FILES['file']['name']);
            $this->collectionsRepo->addCollection($collection);


            $this->render('collections', [
                'collections' => $this->collectionsRepo->getCollections(),
                'message' => $this->messages
            ]);
        } else {
            $this->render('add-collection', ['message' => $this->messages]);
        }
    }


        private
        function validate(array $file): bool
        {
            if ($file['size'] > self::MAX_FILE_SIZE) {
                $this->messages[] = 'FILE IS TOO LARGE!';
                return false;
            }

            if (!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)) {
                $this->messages[] = 'FILE TYPE IS NOT SUPPORTED!';
                return false;
            }
            return true;
        }

    public function collections(){
        $collections = $this->collectionsRepo->getCollections();
        $this->render('collections', ['collections' =>$collections]);
    }
}