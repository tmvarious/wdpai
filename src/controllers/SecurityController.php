<?php

require_once 'AppController.php';
require_once __DIR__ .'/../models/User.php';
require_once __DIR__ .'/../repo/UserRepo.php';

class SecurityController extends AppController {

    private $userRepo;
    public function __construct()
    {
        parent::__construct();
        $this->userRepo = new UserRepo();
    }
    public function login()
    {

        if (!$this->isPost()) {
            $this->render('login');
        }

        $email = $_POST['email'];
        $password = hash('sha512',$_POST['password']);

        $user = $this->userRepo->getUser($email);

        if(!$user){
            $this->render('login', ['messages' => ['User not exist!']]);
        }
        if ($user->getPassword() !== $password) {
            $this->render('login', ['messages' => ['Password is wrong!']]);
        }

        setcookie("id_user", $user->getEmail(), time() + 86400, '/');

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/home");
    }

    public function register(){
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];


        if(empty($email)){
            return $this->render('register', ['messages' => ['Enter your email']]);
        }
        if(empty($password)){
            return $this->render('register', ['messages' => ['Enter your password']]);
        }
        if(empty($confirmedPassword)){
            return $this->render('register', ['messages' => ['Enter your password']]);
        }
        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Passwords are not the same']]);
        }


        $user = new User($email,hash('sha512',$password));

        $check_user = $this->userRepo->getUser($email);
        if ($check_user) {
            return $this->render('register', ['messages' => ['User already exist!']]);
        }

        $this->userRepo->addUser($user);

        return $this->render('login', ['messages' => ['Account created']]);

    }


}