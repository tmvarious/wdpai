<?php

require_once 'AppController.php';
require_once __DIR__ .'/../models/Skins.php';
require_once __DIR__ .'/../repo/SkinsRepo.php';


class SkinsController extends AppController {


    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private $skinsRepo;



    public function __construct()
    {
        parent::__construct();
        $this->skinsRepo = new SkinsRepo();
    }

    public function addSkin()
    {

        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name']
            );

            $price =urlencode($_POST['price']);
            $price_float_value = floatval($price);
            $skin = new Skins($_POST['title'], $price_float_value,  $_FILES['file']['name']);
            $this->skinsRepo->addSkin($skin);


            $this->render('skins', [
                'skins' => $this->skinsRepo->getSkins(),
                'message' => $this->messages
            ]);
        } else {
            $this->render('add-skin', ['message' => $this->messages]);
        }
    }

    public function skins(){
        $skins = $this->skinsRepo->getSkins();
        $this->render('skins', ['skins' =>$skins]);
    }

    public function search(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->skinsRepo->getSkinsByTitle($decoded['search']));
        }

    }

    private function validate(array $file): bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'FILE IS TOO LARGE!';
            return false;
        }

        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'FILE TYPE IS NOT SUPPORTED!';
            return false;
        }
        return true;
    }


}