<?php


class Skins
{
    private $title;
    private $price;
    private $image;


    public function __construct(string $title, float $price, string $image)
    {
        $this->title = $title;
        $this->price = $price;
        $this->image = $image;

    }


    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage($image): void
    {
        $this->image = $image;
    }



}