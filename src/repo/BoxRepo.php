<?php

require_once 'Repo.php';
require_once __DIR__.'/../models/Box.php';



class BoxRepo extends Repo
{

    public function getBox(int $id): ?Box
{

        $stmt = $this->database->connect()->prepare('
                SELECT * FROM cases WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $box = $stmt->fetch(PDO::FETCH_ASSOC);

        if($box == false){
            return null;
        }

        return new Box(
            $box['title'],
            $box['image']
        );
    }

    public function addCase(Box $box): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO cases (title, image)
            VALUES(?, ?)
        ');

        $stmt->execute([
           $box->getTitle(),
           $box->getImage()
        ]);
    }

    public function getBoxes(): array{
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM cases
        ');
        $stmt->execute();
        $boxes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($boxes as $box){
            $result[] = new Box(
                $box['title'],
                $box['image']
            );
        }

        return $result;
    }
}