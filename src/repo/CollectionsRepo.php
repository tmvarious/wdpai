<?php

require_once 'Repo.php';
require_once __DIR__.'/../models/Collections.php';

class CollectionsRepo extends Repo
{

    public function getCollection(int $id): ?Collections
{

        $stmt = $this->database->connect()->prepare('
                SELECT * FROM collections WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $collection = $stmt->fetch(PDO::FETCH_ASSOC);

        if($collection == false){
            return null;
        }

        return new Collections(
            $collection['title'],
            $collection['image']
        );
    }

    public function addCollection(Collections $collection): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO collections (title, image)
            VALUES(?, ?)
        ');

        $stmt->execute([
            $collection->getTitle(),
            $collection->getImage()
        ]);
    }

    public function getCollections(): array{
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM collections
        ');
        $stmt->execute();
        $collcetions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($collcetions as $collcetion){
            $result[] = new Collections(
                $collcetion['title'],
                $collcetion['image']
            );
        }

        return $result;
    }
}