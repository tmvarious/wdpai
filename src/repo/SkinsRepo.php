<?php

require_once 'Repo.php';
require_once __DIR__.'/../models/Skins.php';



class SkinsRepo extends Repo
{

    public function getSkin(int $id): ?Skins
{

        $stmt = $this->database->connect()->prepare('
                SELECT * FROM skins WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $skin = $stmt->fetch(PDO::FETCH_ASSOC);

        if($skin == false){
            return null;
        }

        return new Skins(
            $skin['title'],
            $skin['price'],
            $skin['image']
        );
    }

    public function addSkin(Skins $skin): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO skins (title, price, image)
            VALUES(?, ?, ?)
        ');

        $stmt->execute([
            $skin->getTitle(),
            $skin->getPrice(),
            $skin->getImage()
        ]);
    }

    public function getSkins(): array{
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM skins
        ');
        $stmt->execute();
        $skins = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($skins as $skin){
            $result[] = new Skins(
                $skin['title'],
                $skin['price'],
                $skin['image']
            );
        }

        return $result;
    }

    public function getSkinsByTitle(string $searchString)
    {
        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM skins WHERE LOWER(title) LIKE :search
        ');
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}